// console.log("Pokemon")

let trainers = {
		name: "Ash Kenchu",
		age: 10,
		pokemon: [ "Pikachu","Charizard","Squirtle","Bulbasour"],
		friends: {
				hoenn: ["May", "Max"],
				kanto: ["Brock", "Misty"]
				},
		talk: function(){
			console.log(trainers.pokemon[0]+" I choose you")
		}
	}


console.log(trainers);
trainers.talk();

function Pokemon (name ,level){
		this.pokemonName = name;
		this.pokemonLevel = level;
		this.pokemonHealth = 2 * level;
		this.pokemonAttack = level;
		// tackle
		this.tackle = function(targetPokemon){
			console.log(this.pokemonName + " tackles " + targetPokemon.pokemonName);
			
			let hpAftertackle = targetPokemon.pokemonHealth- this.pokemonAttack;
			
			console.log(targetPokemon.pokemonName + " health is now reduced to "
			 + hpAftertackle);
			
			this.faint = function(){	
						console.log(targetPokemon.pokemonName+ " fainted!");
			}	
			if(hpAftertackle<=0){
				this.faint();
				}

		}
		
	
		 		
			

			
}
let pikachu = new Pokemon("Pikachu", 100);
console.log(pikachu)
let charizard = new Pokemon("Charizard", 50 );
console.log(charizard)
let squirtle = new Pokemon("Squirtle", 50);
console.log(squirtle)
let bulbasour = new Pokemon("Bulbasour", 50);
console.log(bulbasour)


pikachu.tackle(charizard);
